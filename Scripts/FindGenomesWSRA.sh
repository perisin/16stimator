#!/bin/bash
#### This script will search for corresponding SRA reads for NCBI genomes
## searches by TaxID then BioSample then BioProject
#### Input prokaryotes.txt genome table downloaded from NCBI on 11/25/2014
#### Ran on tarbell.cri.uchicago.edu with:
## walltime=00:24:00:00, nodes=1:ppn=1, mem=4gb

##start of script

IFS=$'\n'
set +f
genometab=$1
head -1 $genometab > ./Genomes_SRA.txt
head -1 $genometab > ./Genomes_NoSRA.txt
for line in $( cat $genometab | tail -n +2 )
do
    isolate=$(echo $line | cut -f1)
    echo $isolate
    txid=$(echo $line | cut -f2) 
    echo $txid
    biosamp=$(echo $line | cut -f21)
    echo $biosamp
    bioproj=$(echo $line | cut -f3)
    echo $bioproj
    wget -nv -O ./query_results.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="txid'$txid'"'
    if [ $(grep -P 'Error' query_results.csv | wc -l) -eq 0 ]
    then
	echo 'SRA found with txid'
	cat query_results.csv | tail -n +2 >> ./total_SRA_results.csv
	echo $line >> ./Genomes_SRA.txt
    else
	wget -nv -O ./query_results.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="'$biosamp'"'
	if [ $(grep -P 'Error' query_results.csv | wc -l) -eq 0 ]
	then
	    echo 'SRA found with biosample'
	    cat query_results.csv | tail -n +2 >> ./total_SRA_results.csv
	    echo $line >> ./Genomes_SRA.txt
	else
	    wget -nv -O ./query_results.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="'$bioproj'"'
	    if [ $(grep -P 'Error' query_results.csv | wc -l) -eq 0 ]
	    then
		echo 'SRA found with bioproject'
		cat query_results.csv | tail -n +2 >> ./total_SRA_results.csv
		echo $line >> ./Genomes_SRA.txt
	    else
		echo 'SRA not found'
		echo $line >> ./Genomes_NoSRA.txt
	    fi
	fi
    fi   
done
