#!/bin/bash
#### This script will taxonomically classify sequencing reads with Kraken
#### Input processed sequencing reads that were output of 
## TrimQualCheckFiltPhiX.sh
#### Input a tab-delimited table that specifies the:
## 1st column: Isolate name
## 2nd column: Type of library, either tru (short insert, paired end seqs),
## or mp (long insert, mate paired end seqs)
## 3rd column: read length
#### Ran on tarbell.cri.uchicago.edu with:
## walltime=00:01:00:00, nodes=1:ppn=24, mem=100gb

#### Required:
## perl/5.18.1
## kraken/0.10.4
## built standard kraken database
## >75 gb RAM

##start of script

IFS=$'\n'
set +f
IsoTab=$1
for line in $(tail -n +2 $IsoTab)
do
    Isolate=$(echo $line | cut -f1)
    Lib=$(echo $line | cut -f2)
    ## Taxonomically classify reads
    if test "$Lib" = 'mp'; then
	kraken --db /apps/software/kraken/0.10.4/db/standard-DB/ --threads 24 --fastq-input --output ./Kraken_out/$Isolate.$Lib.KrakenOut.txt --preload --paired ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.rc.fastq ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.rc.fastq
    else
	kraken --db /apps/software/kraken/0.10.4/db/standard-DB/ --threads 24 --fastq-input --output ./Kraken_out/$Isolate.$Lib.KrakenOut.txt --preload --paired ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.fastq ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.fastq
    fi
    ## Generate sample report
    kraken-report --db /apps/software/kraken/0.10.4/db/standard-DB/ ./Kraken_out/$Isolate.$Lib.KrakenOut.txt > ./Kraken_out/$Isolate.$Lib.Krak.Report.txt 
    ## Format sample report
    kraken-mpa-report --db /apps/software/kraken/0.10.4/db/standard-DB/ ./Kraken_out/$Isolate.$Lib.KrakenOut.txt > ./Kraken_out/$Isolate.$Lib.Krak.mpaReport.txt
done
