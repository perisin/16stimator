#!/bin/bash
#### This script will subset NCBI prokaryote genome table and
## create PBS files for batch job submission to estimate 16S copies
#### Input prokaryotes.txt table or subset of same format
#### indicate: 
## number of rows for subset tables
## row to start subsetting in input table
## walltime for job submissions
#### Ran on tarbell.cri.uchicago.edu (this script can be run on head node)

##start of script

IFS=$'\n'
set +f
GenomeTab=$1
Length=$2
Start=$3
Time=$4

NRows=$(wc -l $GenomeTab.txt | grep -P -o '^\d+')
Tabs=$(($NRows/$Length))
End=$(($Start + $Length))

for n in $(seq $Tabs)
do
    if (($End < $NRows))
    then
	head -1 $GenomeTab.txt > $GenomeTab.$n.txt
	sed -n ''$Start', '$End'p' $GenomeTab.txt >> $GenomeTab.$n.txt
	Start=$(($End + 1))
	End=$(($Start + $Length))
    else
	head -1 $GenomeTab.txt > $GenomeTab.$n.txt
	sed -n ''$Start', '$NRows'p' $GenomeTab.txt >> $GenomeTab.$n.txt
    fi
    ## Create PBS file
    echo '#!/bin/bash
###############################
# Resource Manager Directives # 
###############################
#PBS -N SixteenEstimatorPBCI_NCBI_'$GenomeTab'.'$n'
#PBS -S /bin/bash
#PBS -l walltime='$Time'
#PBS -l nodes=1:ppn=4
#PBS -l mem=20gb
#PBS -o $HOME/SixteenEstimatorPBCI_NCBI_'$GenomeTab'.'$n'.log
#PBS -e $HOME/SixteenEstimatorPBCI_NCBI_'$GenomeTab'.'$n'.err
#PBS -m abe
#PBS -M perisin@uchicago.edu
#################
# Job Execution #
#################
# software selection with module
module add R/3.1.0
module add bedtools/2.17.0
module add bowtie2/2.1.0 
module add samtools/0.1.19
module add fastx_toolkit/0.0.13
# the program to be executed
cd /group/bergelson-lab/mperisin/NCBI16S/
bash ./Scripts/Est16S23S_NCBI_PBCI.sh ./Tables/'$GenomeTab'.'$n'.txt '$n'
exit' > /group/bergelson-lab/mperisin/NCBI16S/Scripts/SixteenEstimatorPBCI_NCBI_$GenomeTab.$n.pbs
    qsub /group/bergelson-lab/mperisin/NCBI16S/Scripts/SixteenEstimatorPBCI_NCBI_$GenomeTab.$n.pbs
    sleep 2s
done
