#!/bin/bash
#### This script will assemble processed paired end reads into contigs 
#### Uses VelvetOptimiser.pl script
#### Input Isolate name, library types (either pe, mp, or pemp),
## starting kmer value, ending kmer value,kmer step size, threads
#### Input processed sequencing reads for short (tru) and long (mp) insert
## libraries (generated from TrimQualCheckFiltPhiX.sh)
#### Ran on tarbell.cri.uchicago.edu with:
## walltime=00:04:00:00, nodes=1:ppn=3, mem=30gb

#### Required:
## velvet/1.2.10
## bioperl/1.6.0

##start of script

IFS=$'\n'
set +f

Isolate=$1
Lib=$2
Start=$3
End=$4
Step=$5
Threads=$6

## Check that bioperl is loaded
export PERL5LIB="/home/mperisin/bin/BioPerl-1.6.0:$PERL5LIB"
perl -MBio::Perl -le 'print Bio::Perl->VERSION;'
export PATH=$PATH:~/bin/velvet_1.2.10/

## pemp = both short and long insert libaries
## pe = only short insert
## mp = only long insert
if test "$Lib" = 'pemp'; then
    perl ~/bin/velvet_1.2.10/contrib/VelvetOptimiser-2.2.4/VelvetOptimiser.pl -s $Start -e $End -x $Step -v 1 -p $Isolate -t $Threads -f '-shortPaired -fastq ../Interleaved/'$Isolate'.tru.QualChecked.Shuffled.fastq -shortPaired2 -fastq ../Interleaved/'$Isolate'.mp.QualChecked.Shuffled.fastq' -o '-shortMatePaired2 yes -scaffolding no -min_contig_lgth 100'
fi
if test "$Lib" = 'pe'; then
    perl ~/bin/velvet_1.2.10/contrib/VelvetOptimiser-2.2.4/VelvetOptimiser.pl -s $Start -e $End -x $Step -v 1 -p $Isolate -t $Threads -f '-shortPaired -fastq ../Interleaved/'$Isolate'.tru.QualChecked.Shuffled.fastq' -o '-scaffolding no -min_contig_lgth 100'
fi
if test "$Lib" = 'mp'; then
    perl ~/bin/velvet_1.2.10/contrib/VelvetOptimiser-2.2.4/VelvetOptimiser.pl -s $Start -e $End -x $Step -v 1 -p $Isolate -t $Threads -f '-shortPaired -fastq ../Interleaved/'$Isolate'.mp.QualChecked.Shuffled.fastq' -o '-shortMatePaired yes -scaffolding no -min_contig_lgth 100'
fi
