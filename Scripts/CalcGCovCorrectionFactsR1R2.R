## Based on extracted genome reads and GC%, calculate GC% cov correction factors
CurrentDir = getwd()
setwd(CurrentDir)
## Start code
args = commandArgs(trailingOnly = TRUE)
Isolate = as.character(args[1])
Lib = as.character(args[2])
ReadLen = as.numeric(args[3])
Seq = as.numeric(args[4])
## Load in coverage and GC% results, make sure tables are in same order
GenomeWinCov = read.table(paste("./Beds/", Isolate, ".", Lib, ".R", Seq,
                                ".genome.mapped.reads.txt", sep = ""),
                          sep = "\t", header = FALSE, quote = "",
                          comment.char = "", stringsAsFactors = FALSE)
GenomeWinCov = GenomeWinCov[, (ncol(GenomeWinCov) - 4):ncol(GenomeWinCov)]
colnames(GenomeWinCov) = c("Node", "Start", "End", "Window", "Overlap")
CovWins = data.frame(table(GenomeWinCov$Window), stringsAsFactors = FALSE)
colnames(CovWins) = c("Window", "Reads")

GenomeGC = read.table(paste("./Beds/", Isolate, ".", Lib, ".genome.gc.txt", sep = ""),
                      sep = "\t", header = FALSE, stringsAsFactors = FALSE)
colnames(GenomeGC) = c("Node", "Start", "End", "Window", "pct_at", "pct_gc",
                       "num_A", "num_C", "num_G", "num_T", "num_N", "num_oth",
                       "seq_len")
GCWins = GenomeGC[match(CovWins$Window, GenomeGC$Window),]
CovGCWins = cbind(CovWins, GCWins)
CovGCWins$pct_gc = round(CovGCWins$pct_gc, digits = 2)
## Calculate correction factors for each GC% (Yoon et al, 2009)
MedCov = median(CovGCWins$Reads)
GCprop = seq(0, 100, by = 1)/100
CovCorrect = numeric()
for(m in 1:length(GCprop)){
  CovCorrect[m] = MedCov/median(CovGCWins[CovGCWins$pct_gc == GCprop[m],]$Reads)
}
CovCorrect[CovCorrect == Inf] = 0
CovCorrectDF = data.frame(cbind(GCprop, CovCorrect))
write.csv(CovCorrectDF, paste("./CovCorrect/", Isolate, ".", Lib, ".R", Seq,
                              ".CovCorrectForGC.csv", sep = ""),
          row.names = FALSE)
print("GC correction factors calculated")