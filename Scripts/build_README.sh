#!/bin/bash
## build README from .sh files


touch README.txt
> README.txt

echo -e '##### This file outlines the 16Stimator pipeline \n
## For endophytic de novo assemblies: \n
1. Quality filter sequencing reads \n
Script: TrimQualCheckFiltPhiX.sh \n' >> README.txt

awk '/^##/,/##start/' TrimQualCheckFiltPhiX.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt

echo -e '2. Check for contamination \n
Script: KrakenCheckProcessedReads.sh \n' >> README.txt

awk '/^##/,/##start/' KrakenCheckProcessedReads.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt


echo -e '3. de novo assemble genomes \n
Script: VelvetOptimAssemble.sh' >> README.txt

awk '/^##/,/##start/' VelvetOptimAssemble.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt


echo -e '4. Estimate 16S copy number with Price & Bonett confidence
intervals \n
Script: SixteenEstimator_PriceBonCI.sh \n
Subscripts: ParseRASTTableForGeneLocs.R; CreateGenomeGeneBeds.R;
CalcGCovCorrectionFactsR1R2.R; Estimate_16S23S_PriceBonCIR1R2.R;
Estimate_16S23S_PriceBonCIR1R2_GCLM.R' >> README.txt


awk '/^##/,/##start/' SixteenEstimator_PriceBonCI.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt



echo -e '5. Estimate 16S copy number with permutation based confidence \n
intervals\n
Script: SixteenEstimator_Perms.sh \n
Subscripts: CreateGeneBedsForPerms.R; Estimate_16S23S_PermCIR1R2.R' >> README.txt

awk '/^##/,/##start/' SixteenEstimator_Perms.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt



echo -e '## For NCBI genomes: \n
1. Find genomes that have SRA reads \n
Script: FindGenomesWSRA.sh \n' >> README.txt


awk '/^##/,/##start/' FindGenomesWSRA.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt




echo -e '2. Estimate 16S copy number with Price & Bonett confidence
intervals \n
Script: Est16S23S_NCBI_PBCI.sh \n
Subscripts: CreateGenomeGeneBedsNCBI.R; \n
CalcGCovCorrectionFactsR1R2NCBI.R; \n
Estimate_16S23S_PriceBonCIR1R2NCBI.R; \n
Estimate_16S23S_PriceBonCIR1R2NCBI_GCLM.R \n' >> README.txt

awk '/^##/,/##start/' Est16S23S_NCBI_PBCI.sh | sed '/##start/,/+1000/d' |
sed -e $'s/#### /\\\n/g' | sed -e $'s/## //g' >> README.txt
