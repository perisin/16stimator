#!/bin/bash
#### This script will trim and quality check Illumina sequencing reads prior to
## genome assembly
#### Steps include: check initial read quality; trim reads for quality and adapter
## sequences; filter reads that align to PhiX; check quality of processed reads;
## reverse complement if mate pair reads; interleave reads
#### Input a tab-delimited table that specifies the:
## 1st column: Isolate name
## 2nd column: Type of library, either tru (short insert, paired end seqs),
## or mp (long insert, mate paired end seqs)
## 3rd column: read length
#### Paired end seqs need to be named $Isolate.$Lib.R1.fastq & $Isolate.$Lib.R2.fastq
#### Ran on tarbell.cri.uchicago.edu with:
## walltime=00:04:00:00, nodes=1:ppn=8, mem=24gb

#### Required:
## fastqc/0.10.1 
## java/jdk1.7.0_45
## trimmomatic-0.32.jar
## bowtie2/2.1.0
## fastx_toolkit/0.0.13
## perl/5.18.1
## shuffleSequences_fastq.pl from velvet/1.2.10
## illumina_seqs_to_trim.fa contains Illumina adapter seqs
## bowtie2 indexed PhiX genome sequence

##start of script

IFS=$'\n'
set +f
IsoTab=$1
for line in $(tail -n +2 $IsoTab)
do
    Isolate=$(echo $line | cut -f1)
    Lib=$(echo $line | cut -f2)
    ## Check raw sequence quality
    fastqc ./Seqs/$Isolate.$Lib.R1.fastq ./Seqs/$Isolate.$Lib.R2.fastq --outdir=./Fastqc_out/
    ## Trim Illumina adapters and quality filter
    ## illumina_seqs_to_trim.fa contains Illumina adapter seqs
    java -jar ~/bin/trimmomatic-0.32.jar PE -threads 8 -phred33 -trimlog ./Trimmed/$Isolate.$Lib.log ./Seqs/$Isolate.$Lib.R1.fastq ./Seqs/$Isolate.$Lib.R2.fastq ./Trimmed/$Isolate.$Lib.R1.pe.trim.fastq ./Trimmed/$Isolate.$Lib.R1.se.trim.fastq ./Trimmed/$Isolate.$Lib.R2.pe.trim.fastq ./Trimmed/$Isoalte.$Lib.R2.se.trim.fastq ILLUMINACLIP:./illumina_seqs_to_trim.fa:2:30:7 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:81 CROP:81
    rm ./Trimmed/$Isolate.$Lib.R1.se.trim.fastq
    rm ./Trimmed/$Isolate.$Lib.R2.se.trim.fastq
    ## Filter PhiX reads
    bowtie2 -x ./PhiX/phix -q --phred33 -1 ./Trimmed/$Isolate.$Lib.R1.pe.trim.fastq -2 ./Trimmed/$Isolate.$Lib.R2.pe.trim.fastq --un-conc ./TrimPhiXFilt/$Isolate.$Lib.R%.pe.trim.nophix.fastq -S ./TrimPhiXFilt/$Isolate.$Lib.phixAligned.sam -p 8
    rm ./TrimPhiXFilt/$Isolate.$Lib.phixAligned.sam
    ## Check processed sequence quality
    fastqc ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.fastq ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.fastq --outdir=./Fastqc_out/
    ## Reverse complement mate pair reads
    if test "$Lib" = 'mp'; then
	fastx_reverse_complement -Q33 -i ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.fastq -o ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.rc.fastq
	fastx_reverse_complement -Q33 -i ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.fastq -o ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.rc.fastq
	rm ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.fastq
	rm ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.fastq
	## Interleave reads
	perl ~/bin/velvet_1.2.10/contrib/shuffleSequences_fasta/shuffleSequences_fastq.pl ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.rc.fastq ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.rc.fastq ./Interleaved/$Isolate.$Lib.QualChecked.Shuffled.fastq
    else
	perl ~/bin/velvet_1.2.10/contrib/shuffleSequences_fasta/shuffleSequences_fastq.pl ./TrimPhiXFilt/$Isolate.$Lib.R1.pe.trim.nophix.fastq ./TrimPhiXFilt/$Isolate.$Lib.R2.pe.trim.nophix.fastq ./Interleaved/$Isolate.$Lib.QualChecked.Shuffled.fastq
    fi   
done
