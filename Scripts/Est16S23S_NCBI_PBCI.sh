#!/bin/bash
#### This script will estimate 16S rRNA gene copy numbers via
## a read-depth approach, as was applied to Endophyte genomes
## estimates are ratio of medians for read depth for 16S/SingleCopyGene
## First estimates will correct for GC bias for read depth based on Yoon et al. 2009
## Last R script will apply a GC correction based only if there is a significant
## linear relationshp between single copy gene read depth and GC%
## includes confidence intervals (Price & Bonett, 2002)
#### Input subsetted prokaryotes.txt genome table from NCBI (downloaded on 11/25/2014)
## subsetted for genomes with SRA records (from FindGenomesWSRA.sh)
## also indicate a job number
#### Ran on tarbell.cri.uchicago.edu with:
## walltime depends on how many genomes
## can create subset genome tables and create PBS files with CreateSubsetNCBITabs.sh
## nodes=1:ppn=4, mem=20gb

#### Required:
## R/3.1.0
## bedtools/2.17.0
## bowtie2/2.1.0 
## samtools/0.1.19
## fastx_toolkit/0.0.13
## sratoolkit.2.3.5-2-ubuntu64

##start of script

IFS=$'\n'
set +f
shopt -s extglob
SRAgenomes=$1
Job=$2
MainDir=$(echo '/group/bergelson-lab/mperisin/NCBI16S/')
cd $MainDir
for line in $(cat $SRAgenomes | tail -n +2)
do
    ## Load in genome info
    ## get rid of weird characters in isolate name
    isolate=$(echo $line | cut -f1 | sed 's/\s/_/g' | sed 's/\//_/g' | sed 's/\*/_/g' | sed 's/[()]/_/g' | sed 's/\[/_/g' | sed 's/\]/_/g' | sed 's/[-=,.#:+]/_/g')
    echo $isolate
    txid=$(echo $line | cut -f2) 
    echo $txid
    biosamp=$(echo $line | cut -f21)
    echo $biosamp
    bioproj=$(echo $line | cut -f3)
    echo $bioproj
    ## Find corresponding SRA records 
    wget -nv -O ./$isolate.$bioproj.SRA.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="txid'$txid'"'
    if [ $(grep -P 'Error' $isolate.$bioproj.SRA.csv | wc -l) -eq 0 ]
    then
	echo 'SRA found with txid'
	cat $isolate.$bioproj.SRA.csv | tail -n +2 >> ./totalSRAresults.csv
    else
	wget -nv -O ./$isolate.$bioproj.SRA.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="'$biosamp'"'
	if [ $(grep -P 'Error' $isolate.$bioproj.SRA.csv | wc -l) -eq 0 ]
	then
	    echo 'SRA found with biosample'
	    cat $isolate.$bioproj.SRA.csv| tail -n +2 >> ./totalSRAresults.csv
	else
	    wget -nv -O ./$isolate.$bioproj.SRA.csv 'http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term="'$bioproj'"'
	    if [ $(grep -P 'Error' $isolate.$bioproj.SRA.csv | wc -l) -eq 0 ]
	    then
		echo 'SRA found with bioproject'
		cat $isolate.$bioproj.SRA.csv | tail -n +2 >> ./totalSRAresults.csv
	    else
		echo 'SRA not found'
	    fi
	fi
    fi 
    if [ $(grep -P 'Error' $isolate.$bioproj.SRA.csv | wc -l) -eq 0 ]
    then
	## Create a working directory
	storage1=$(echo '/group/bergelson-lab/mperisin/NCBI16S/IsoFolds/'$isolate.$bioproj'')
	mkdir $storage1
	mv $isolate.$bioproj.SRA.csv $storage1/
	cd $storage1
	head -1 $MainDir/$SRAgenomes | sed 's/#//g' > Current_genome.txt
	echo $line >> Current_genome.txt
	## Download the genome fasta
	genomeftp=$(echo $line | cut -f24)
	if test "$genomeftp" != '-'; then
	    wget -nv 'ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_BACTERIA/'$genomeftp'/*.fna*'
	    if(( $(ls *.fna* | grep -P -o '.tgz' | wc -l) > 0))
	    then
		tar -xzf *.fna*
	    fi
	    cat *.fna > $isolate.$bioproj.genome.fna
	    ## build bowtie2 indices for genome assembly
	    bowtie2-build -q $isolate.$bioproj.genome.fna $isolate.$bioproj
	    samtools faidx $isolate.$bioproj.genome.fna
	    ## Download ptt and rnt tables (annotations)
	    wget -nv 'ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_BACTERIA/'$genomeftp'/*.ptt*'
	    if(( $(ls *.ptt* | grep -P -o '.tgz' | wc -l) > 0))
	    then
		tar -xzf *.ptt*
	    fi
	    wget -nv 'ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_BACTERIA/'$genomeftp'/*.rnt*'
	    if(( $(ls *.rnt* | grep -P -o '.tgz' | wc -l) > 0))
	    then
		tar -xzf *.rnt*
	    fi
	    ## Find rRNA and conserved gene locations
	    echo 'Finding gene positions'
	    for geneline in $( cat $MainDir/Conserved_gene_list.txt )
	    do
		grep -P "\t$geneline$" *.ptt *.rnt >> $isolate.$bioproj.Conserved_gene_locs_table.txt
	    done
	    genelines=$(wc -l $isolate.$bioproj.Conserved_gene_locs_table.txt | grep -P -o '^\d+')
	    if(( $genelines > 0 ))
	    then
		echo 'Found gene positions'
		## Download SRA reads, map reads to genome, and calculate coverage 
		for sraline in $( cat $isolate.$bioproj.SRA.csv | tail -n +2 )
		do 
		    head -1 $isolate.$bioproj.SRA.csv > Current_SRA.csv
		    echo ''$sraline'' >> Current_SRA.csv
		    sraname=$(echo $sraline | cut -d "," -f1)
		    sraftp=$(echo $sraline | cut -d "," -f10)
		    sralen=$(echo $sraline | cut -d "," -f7)
		    srastrat=$(echo $sraline | cut -d "," -f13)
		    sralib=$(echo $sraline | cut -d "," -f16)
		    ## only want whole genome sequencing projects
		    if test "$srastrat" = 'WGS'; then
			wget -nv -O $sraname.sra ''$sraftp''
			echo ''$sraname' dowloaded'
			## Convert SRA to fastq
			/home/mperisin/bin/sratoolkit.2.3.5-2-ubuntu64/bin/fastq-dump -M 20 --split-files $sraname.sra -O ./
			echo ''$sraname' unpacked'
			rm $sraname.sra
			## Run R script to correct gene table names and create genome and gene beds
			## Create bed files by windowing over the genome and genes 
			## window size = 2 * ReadLen
			## requires: genome fasta and gene positions
			Rscript $MainDir/Scripts/CreateGenomeGeneBedsNCBI.R $isolate $bioproj $sralen
			## Calculate GC% for each genome and gene window
			bedtools nuc -fi ./$isolate.$bioproj.genome.fna -bed ./$isolate.$bioproj.genome.gc.bed > ./$isolate.$bioproj.genome.gc.txt
			bedtools nuc -fi ./$isolate.$bioproj.genome.fna -bed ./$isolate.$bioproj.gene.gc.bed > ./$isolate.$bioproj.gene.gc.txt
			echo 'Genome and gene GC %s extracted'
			## Extract read number and crop all reads to the same length
			for num in $(ls $sraname*.fastq | grep -P -o '_\d+' | sed 's/_//g')
			do
			    fastx_trimmer -Q33 -l $sralen -i ''$sraname''_$num.fastq -o ''$sraname''_$num.trim.fastq
			    rm ''$sraname''_$num.fastq
			    ## Align reads against genome assembly
			    bowtie2 -x $isolate.$bioproj -p 4 -q -U ''$sraname''_$num.trim.fastq -S ''$sraname''_$num.align.sam 2> ''$sraname''_$num.bowtie.txt
			    echo ''$sraname' R'$num' aligned'
			    rm ''$sraname''_$num.trim.fastq
			    ## Extract total number of reads and proportion that aligned well
			    total=$(grep -P 'reads; of these' ''$sraname''_$num.bowtie.txt | grep -P -o '\d+')
			    prop=$(grep -P 'overall alignment rate' ''$sraname''_$num.bowtie.txt | grep -P -o '^\d+')
			    if (( $prop > 50 ))
			    then
				echo ''$sraname' R'$num' aligned well'
				## Convert sam to bam
				samtools import $isolatename.$bioproj.genome.fna.fai ''$sraname''_$num.align.sam ''$sraname''_$num.align.bam
				echo ''$sraname' R'$num' sam to bam'
				rm ''$sraname''_$num.align.sam
				echo 'Total,Prop' > ''$sraname''_$num.mapped.csv
				echo ''$total,$prop'' >> ''$sraname''_$num.mapped.csv
				## Calculate read depth for each genome and gene window
				bedtools intersect -abam ''$sraname''_$num.align.bam -b $isolate.$bioproj.genome.bed -wo -bed > $isolate.$bioproj.$sraname.$num.genome.mapped.reads.txt
				bedtools intersect -abam ''$sraname''_$num.align.bam -b $isolate.$bioproj.gene.bed -wo -bed > $isolate.$bioproj.$sraname.$num.gene.mapped.reads.txt
				echo ''$sraname' R'$num' coverage calculated for genome and genes'
				rm ''$sraname''_$num.align.bam
				## Based on genome GC% and read depth
				## calculate GC% correction factors for read depth (Yoon et al., 2009)
				Rscript $MainDir/Scripts/CalcGCovCorrectionFactsR1R2NCBI.R $isolate $bioproj $sraname $num
				## Estimate 16S, 23S copy numbers as ratio of median GC corrected read depths 
				## with confidence intervals by Price & Bonett, 2002
				Rscript $MainDir/Scripts/Estimate_16S23S_PriceBonCIR1R2NCBI.R $isolate $bioproj $sraname $num
				## Move files to keep
				mv *.GCorCovPerGeneWin.csv $MainDir/RDPerGeneTabs/
				mv *.CopyNumEst.csv $MainDir/FinalEstimates/
				rm $isolate.$bioproj.$sraname.$num.genome.mapped.reads.txt
				rm $isolate.$bioproj.$sraname.$num.gene.mapped.reads.txt
				rm $isolate.$bioproj.$sraname.$num.CovCorrectForGC.csv
			    else
				## Record poor alignments
				echo ''$sraname' R '$num' aligned poorly'
				echo $sraline >> $MainDir/SRA_bad_align.csv
				rm ''$sraname''_$num.align.sam
			    fi
			done
		    else
			## Record whether WGS
			echo ''$sraname' not WGS'
			echo $sraline >> $MainDir/SRA_not_WGS.csv
		    fi
		done
	    else
		## Record whether annotated
		echo 'Genome not annotated'
		echo $line >> $MainDir/Genomes_NoAnnot.txt
	    fi
	else
	    ## Record whether assembly FTP link in table
	    echo 'No genome ftp'
	    echo $line >> $MainDir/NoGenomeFTP.txt
	fi   
	## Cleanup 
	cd $MainDir/
	rm -r $storage1
    fi
    ## Run updated GC correction method that only uses single copy gene
    ## to test for linear bias in read-depth
    Rscript $MainDir/Scripts/Estimate_16S23S_PriceBonCIR1R2NCBI_GCLM.R
done

