#!/bin/bash
#### This script will estimate 16S rRNA gene copy numbers via
## a read-depth approach, applied to Endophyte genomes
## estimates are based on picking random positions in 16S, 23S, and
## single copy genes and dividing read depths (GC corrected or not)
#### This script assumes SixteenEstimator_PriceBonCI.sh has already been run
#### Input a tab-delimited table that specifies the:
## 1st column: Isolate name
## 2nd column: Type of library, either tru (short insert, paired end seqs),
## or mp (long insert, mate paired end seqs)
## 3rd column: read length
#### Ran on tarbell.cri.uchicago.edu with:
## walltime=00:10:00:00, nodes=1:ppn=4, mem=20gb

#### Required:
## R/3.1.0
## bedtools/2.17.0

##start of script

IFS=$'\n'
set +f
IsoTab=$1
for line in $(tail -n +2 $IsoTab)
do
    Isolate=$(echo $line | cut -f1)
    Lib=$(echo $line | cut -f2)
    ReadLen=$(echo $line | cut -f3)
    echo 'Processing '$Isolate' '$Lib' with read length '$ReadLen''
    ## Create bed files by windowing over the genome
    ## Create bed file for 16S, 23S and SingCop genes, include window for 
    ## for each window
    ## window size = 2 * ReadLen
    ## requires: genome fasta and gene positions
    Rscript ./Scripts/CreateGeneBedsForPerms.R $Isolate $Lib $ReadLen 
    ## Calculate GC% for each genome and gene window
    bedtools nuc -fi ./Genomes/$Isolate.contigs.fa -bed ./Beds/$Isolate.$Lib.gene.gc.perms.bed > ./Beds/$Isolate.$Lib.gene.gc.perms.txt
    echo 'Gene GC %s extracted'
    for n in $(seq 1 2)
    do
	## Calculate read depth for each genome and gene window
	bedtools intersect -abam ./Bams/$Isolate.$Lib.R$n.bam -b ./Beds/$Isolate.$Lib.gene.perms.bed -wo -bed > ./Beds/$Isolate.$Lib.R$n.gene.mapped.reads.perms.txt
	echo ''$Lib' R'$n' coverages for gene extracted'
	## Estimate 16S, 23S copy numbers by randomly picking positions in
	## 16S, 23S and SingCop genes and dividing GC corrected read depths
	## permute to get distribution and confidence intervals
	Rscript ./Scripts/Estimate_16S23S_PermCIR1R2.R $Isolate $Lib $ReadLen $n
    done
done
